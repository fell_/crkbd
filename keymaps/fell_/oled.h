#ifdef OLED_ENABLE

void oled_layer(void);
void oled_mods(void);
void oled_caps(void);
void oled_logo(void);

#endif // OLED_ENABLE
