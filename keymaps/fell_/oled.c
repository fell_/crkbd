#include "stdio.h"
#include "action_layer.h"
#include "action_util.h"
#include "host.h"
#include "lib.h"
#include "oled_driver.h"

#ifdef OLED_ENABLE

static const char PROGMEM logo[]        = {0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0};
static uint8_t            mod_bit_xor[] = {MOD_BIT_LCTRL ^ MOD_BIT_RCTRL, MOD_BIT_LSHIFT ^ MOD_BIT_RSHIFT, MOD_BIT_LALT ^ MOD_BIT_RALT, MOD_BIT_LGUI ^ MOD_BIT_RGUI};
static char              *mod_bit_str[] = {"C", "S", "A", "G"};

void oled_layer(void) {
    oled_write_P(PSTR("Layer: "), false);

    switch (get_highest_layer(layer_state)) {
        case 0:
            oled_write_ln_P(PSTR("Default"), false);
            break;
        case 1:
            oled_write_ln_P(PSTR("Symbol"), false);
            break;
        case 2:
            oled_write_ln_P(PSTR("Number"), false);
            break;
        default:
            oled_write_ln_P(PSTR("NULL"), false);
            break;
    }
}

void oled_mods(void) {
    oled_write_P(PSTR("Mods:  "), false);

    uint8_t mods   = get_mods();
    bool    active = false;
    char    buf[4];

    for (int i = 0; i < 4; i++) {
        if (mod_set(mods, mod_bit_xor[i])) {
            active = true;
            sprintf(buf + strlen(buf), "%s", mod_bit_str[i]);
        }
    }

    oled_write_ln_P(active ? buf : "NULL", false);
}

void oled_caps(void) {
    led_t state = host_keyboard_led_state();
    oled_write_P(PSTR("Caps:  "), false);
    oled_write_ln_P(state.caps_lock ? PSTR("On") : PSTR("Off"), false);
}

void oled_logo(void) {
    oled_write_P(logo, false);
}

#endif // OLED_ENABLE
