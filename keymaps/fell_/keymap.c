/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "keymap_german.h"
#include "lib.h"
#include "oled.h"
#include "oled_driver.h"
#include "os_detection.h"
#include "quantum_keycodes.h"
#include QMK_KEYBOARD_H

os_variant_t OS = OS_UNSURE;

enum key_codes {
    KC_SORSS = SAFE_RANGE,
};

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT_split_3x6_3(
        KC_TAB,  DE_Q, DE_W,     DE_E, DE_R, DE_T, /*|  |*/ DE_Z, DE_U, DE_I,    DE_O,   DE_P,    DE_UDIA,
        KC_ESC,  DE_A, KC_SORSS, DE_D, DE_F, DE_G, /*|  |*/ DE_H, DE_J, DE_K,    DE_L,   DE_ODIA, DE_ADIA,
        KC_LSFT, DE_Y, DE_X,     DE_C, DE_V, DE_B, /*|  |*/ DE_N, DE_M, DE_COMM, DE_DOT, DE_MINS, KC_LALT,
        // ----------------------------------------/*|  |*/-------------------------------------------- //
                  KC_LCTL, LGUI_T(KC_SPC), OSL(1), /*|  |*/ OSL(2), KC_BSPC, KC_ENT
    ),

    [1] = LAYOUT_split_3x6_3(
        KC_TRNS, DE_LABK, DE_LBRC, DE_LCBR, DE_LPRN, DE_TILD, /*|  |*/ DE_PIPE, DE_RPRN, DE_RCBR, DE_RBRC, DE_RABK, KC_NO,
        KC_TRNS, DE_SECT, DE_0,    DE_PERC, DE_BSLS, DE_DQUO, /*|  |*/ DE_QUOT, DE_SLSH, DE_AMPR, DE_DLR,  DE_EQL,  KC_NO,
        KC_CAPS, KC_NO,   DE_AT,   DE_ACUT, DE_HASH, DE_EXLM, /*|  |*/ DE_QUES, DE_ASTR, DE_GRV,  DE_CIRC, DE_PLUS, KC_NO,
        // ---------------------------------------------------/*|  |*/------------------------------------------------- //
                                     KC_TRNS, KC_TRNS, TG(1), /*|  |*/ KC_TRNS, KC_TRNS, KC_TRNS
    ),

    [2] = LAYOUT_split_3x6_3(
        KC_TRNS, KC_NO, KC_NO, KC_F11, KC_F12, KC_NO, /*|  |*/ KC_MUTE, KC_VOLD, KC_VOLU, KC_NO,    KC_NO, KC_NO,
        KC_TRNS, DE_1,  DE_2,  DE_3,   DE_4,   DE_5,  /*|  |*/ KC_LEFT, KC_DOWN, KC_UP,   KC_RIGHT, KC_NO, KC_NO,
        KC_TRNS, DE_6,  DE_7,  DE_8,   DE_9,   DE_0,  /*|  |*/ KC_MPLY, KC_MPRV, KC_MNXT, KC_NO,    KC_NO, KC_NO,
        // -------------------------------------------/*|  |*/------------------------------------------------ //
                           KC_TRNS, KC_TRNS, KC_TRNS, /*|  |*/ TG(2), KC_TRNS, KC_TRNS
    ),
};
// clang-format on

#ifdef OLED_ENABLE

bool oled_task_user() {
    if (is_keyboard_master()) {
        oled_set_cursor(0, 0);
        oled_layer();
        oled_mods();
        oled_caps();
    } else {
        oled_logo();
    }

    return false;
}

#endif

void keyboard_pre_init_user() {
    setPinOutput(24);
    writePinHigh(24);
}

bool process_detected_host_os_user(os_variant_t detected_os) {
    OS = detected_os;
    return true;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case KC_SORSS:
            send_key(mod_set(get_mods(), MOD_BIT_LALT ^ MOD_BIT_RALT) ? DE_SS : DE_S, record);
            return false;

        case KC_ESC:
        case KC_CAPS:
            if (OS != OS_LINUX) return true;
            send_key(keycode == KC_ESC ? KC_CAPS : KC_ESC, record);
            return false;
    }

    return true;
}
