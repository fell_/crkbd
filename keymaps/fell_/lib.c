#include "action.h"

void send_key(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed) {
        register_code(keycode);
    } else {
        unregister_code(keycode);
    }
}

bool mod_set(uint8_t mods, uint8_t mod_bit) {
    return (mods & mod_bit) != 0;
}
